PLATYPUS（1）BSD通用命令手册PLATYPUS（1）

名称

     platypus - 从命令行脚本创建macOS应用程序包。

概要

     platypus [OPTIONS] scriptPath  [destinationPath]

描述

     platypus是Platypus Mac应用程序的命令行。
     它从命令行脚本创建macOS应用程序包。看到
     http://sveinbjorn.org/platypus了解详情。

     platypus默认创建应用程序包，但也可以创建
     可以通过命令行程序加载的Platypus配置文件
     鸭嘴兽的应用程序。

     如果scriptPath参数为“ - ”，则从STDIN读取脚本文本。

     支持以下选项标志（按顺序解析）
     他们通过了）：

     -O， - 生成配置文件
          创建配置文件而不是创建应用程序包。
          启用此选项时，“destinationPath”参数（即
          程序的最后一个参数）应该有一个.platypus后缀。
          如果字符串' - '作为目标路径提供，则配置文件prop-
          erty list XML将被转储到STDOUT。

     -P， -  load-profile  profilePath
          从Platypus配置文件加载所有设置。它还是
          必须为应用程序指定目标路径。Subse-
          quent参数可以覆盖配置文件设置。

     -a， -  name  appName
          指定应用程序的名称。这可以是不同的
          从.app包本身的名称，并显示在
          应用程序的菜单，“关于”窗口和Info.plist属性列表。

     -o， -  interface -type interfaceType
          指定应用程序的接口类型，它应该是其中之一
          下列：

          '无'应用程序不显示任何用户界面，除了
          用于Dock图标和菜单。

          '进度 条'应用程序在运行时显示进度条
          宁。

          '文本 窗口'应用程序显示带有文本字段的窗口
          所有脚本输出。

          'Web  View'应用程序显示一个带有Web视图的窗口
          将脚本输出呈现为HTML。

          '状态 菜单'应用程序在中显示状态菜单项
          菜单栏运行脚本并在单击时显示其输出。

          'Droplet'应用程序显示一个用于删除的Droplet窗口
          文件以供脚本处理。

          默认接口类型是“文本窗口”。

     -i， -  app-icon  iconPath
          指定用作应用程序包图标的文件。该
          文件必须是Apple .icns文件。如果未指定，则为默认值
          将使用鸭嘴兽app图标。如果指定，但留空（''），
          不会为应用设置应用程序图标。

     -Q， -  document-icon  iconPath
          指定用作应用程序文档的图标的文件。
          必须是Apple .icns文件。

     -p，--  interpreter  interpreterPath
          设置脚本解释器（例如/ usr / bin / python或/ bin / sh）。如果
          未指定解释器，命令行工具将尝试
          猜猜正确的翻译。如果失败，则默认shell
          使用解释器/ bin / sh。

     -V， -  app-version  版本
          设置应用程序包的版本。这显示在
          Info.plist属性列表和关于窗口。

     -u， --author  AUTHORNAME
          设置应用程序作者的名称（例如“Apple Computer”或
          “约翰·史密斯”）。如果未指定，则默认为当前
          用户的完整用户名。

     -f， -  bundle-file  filePath
          指定要与应用程序捆绑在一起的文件。该文件将
          被复制到应用程序包的Res​​ources文件夹，
          这是脚本运行的文件夹。任意数量的
          文件可以这种方式捆绑在一起。

     -I， -  bundle-identifier  bundleIdentifier
          设置应用程序的包标识符。应用程序标识符
          是一个反向DNS名称（例如com.apple.iTunes），它唯一地标识
          打败申请。如果此选项保留为空，则默认为该选项
          到“org.username.appname”格式的标识符（例如
          org.sveinbjorn.Platypus）。

     -A， -  admin-privileges
          此标志通过应用程序请求管理员权限
          Apple的安全框架（即提示输入密码）然后
          使用这些权限执行脚本。有关性质的详细信息
          这些权限，请参阅授权的Apple文档
          Security.framework中的tionExecuteWithPrivileges（）。这不是
          严格等同于以root身份运行。

     -D， -  droppable
          使应用程序可以放置，即能够接收拖动
          并删除文件作为脚本的参数。应用包 - 
          修改了dle的属性列表，以便它可以接收丢弃的文件
          在Dock和Finder中。然后将这些文件传递给脚本
          作为参数。

     -F， -  text-droppable
          使应用程序文本可以放置，即使其接受拖动
          文本片段，然后通过STDIN传递给脚本。

     -N， -  服务
          使应用程序注册为可从Ser-访问的动态服务
          恶习申请子菜单。

     -B， -  背景
          此选项使应用程序在后台运行，因此它
          图标不会出现在Dock中。这是通过注册来完成的
          使用LaunchServices作为用户界面元素的应用程序
          （LSUIElement）。

     -R， - 执行后 - 执行
          此选项使应用程序在脚本执行后退出
          cuted。

     -X， --suffixes  后缀
          仅在应用程序接受删除文件时才相关。这面旗帜
          指定应用程序的文件后缀（例如.txt，.wav）
          可以打开。这应该是一个| -separated字符串（例如“txt | wav | jpg”）。

     -T， -  uniform-type-identifiers  utis
          仅在应用程序接受删除文件时才相关。这面旗帜
          指定应用程序的统一类型标识符（UTI）
          可以打开。这应该是一个|  - 分隔的字符串（例如“pub-
          lic.item | public.folder“）。如果使用此标志，则忽略后缀。

     -U， - 个人计划 方案
          将应用程序设置为URI方案的处理程序 这些可以是
          标准的URI方案，如你的http或自定义URI方案
          选择。请参阅文档了解详细信 多个项目应该是a
          |  - 分隔字符串（例如“ftp | myscheme | someotherscheme”）。

     -Z， -  file-prompt
          应用程序启动时显示“打开文件”对话框。

     -G，--  interpreter-args  参数
          脚本解释器的参数。这些应指定为
          a |  - 分隔字符串（例如'-w | -s | -l'）。

     -C， -  script-args  参数
          脚本的参数。这些应指定为| -sepa-
          额定字符串（例如'-w | -s | -l'）。

     -b， -  text -background-color hexColor
          设置文本的背景颜色（例如#ffffff）。

     -g， -  text -foreground-color hexColor
          设置文本的前景色（例如＃000000）。

     -n， -  text-font  fontName
          设置文本视图的字体和字体大小（例如'Monaco 10'）。

     -K， -  status-item-kind  kind
          仅适用于“状态菜单”界面类型 设置Status的显示种类
          菜单界面类型。这可以是“文字”或“图标”。

     -Y， -  status-item-title  title
          仅适用于“状态菜单”界面类型 设置的显示标题
          状态菜单界面类型中的状态项。

     -L， -  status-item-icon  imagePath
          仅适用于“状态菜单”界面类型 设置图标图像
          状态菜单界面类型中的状态项。必须是图像文件
          Cocoa API支持的格式之一（例如PNG，JPEG，TIFF
          等等。）

     -c， -  status-item-sysfont
          仅适用于“状态菜单”界面类型 使菜单使用系统字体
          而不是用户定义的样式。

     -d， -  symlink
          在应用程序内部创建原始脚本的符号链接
          捆绑而不是复制脚本。还创建了符号链接
          任何捆绑的文件。

     -l， -  upload-nib
          剥离捆绑的应用程序nib文件以减小其大小。做出来的
          笔尖不可编辑。仅在安装了Apple的XCode时才有效。

     -y， -  覆盖
          覆盖目标路径中的任何预先存在的文件或文件夹。

     -v， -  version
          打印此程序的版本

     -h， -  help
          打印帮助和用法字符串

     成功时退出0，如果发生错误则退出> 0。

FILES

     / usr / local / bin / platypus程序二进制文件
     / usr / local / share / platypus / ScriptExec可执行二进制文件
     应用程序的/usr/local/share/platypus/MainMenu.nib nib文件
     /usr/local/share/platypus/PlatypusDefault.icns默认图标

例子

     platypus -P myProfile.platypus~ / Desktop / MyApplication.app

     platypus -o'文本窗口'script.pl PerlScript.app

     platypus -a'Me app'-p / usr / bin / python myPythonScript.py

     platypus -D -a MyDroplet -o'Droplet'~ / droplet.sh

作者

     Sveinbjorn Thordarson <sveinbjorn@sveinbjorn.org>

达尔文2018年11月25日达尔文
